# TimeOff.Management Deployment to a public AWS

As part of the assignment, the application is deployed to AWS, running on a container orchestrated by a Fargate cluster.

The application is running and accessible on the following URL: https://gorilla.devgameops.com

The following diagram will hopefully summarize the architecture used to run the application: 

![Architecture diagram](https://bitbucket.org/giovesgo/timeoffmgmt/raw/cc45821e29a6ab69b4d2271b023d9b91695b8892/docs/architecture.drawio.png)

The code repository is hosted on Bitbucket, and the CI/CD is provided with Bitbucket Pipelines.

For the repository structure I followed a monorepo pattern. There is a folder for the infrastructure, defined with Terraform.
There is a folder for the application, as taken from Github and with some modifications to make it more flexible to run on a container
on the cloud.

I had to take some shortcuts, the implementation is a bit raw but the spirit of the task has been accomplished (I hope).


