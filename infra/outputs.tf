output "vpc" {
  description = "VPC created to hold Hasura resources"
  value = aws_vpc.gorilla
}

output "private_subnets" {
  description = "Private subnets created for RDS within the VPC, each in a different AZ"
  value = aws_subnet.gorilla_private
}

output "public_subnets" {
  description = "Public subnets created for Hasura within the VPC, each in a different AZ"
  value = aws_subnet.gorilla_public
}

output "ecs_security_group" {
  description = "Security group controlling access to the ECS tasks"
  value = aws_security_group.gorilla_ecs
}