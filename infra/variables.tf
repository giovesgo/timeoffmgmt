variable "region" {
  description = "Region to deploy"
  default     = "us-east-1"
}

variable "domain" {
  description = "Domain name. Service will be deployed using the gorilla_subdomain"
}

variable "gorilla_subdomain" {
  description = "The Subdomain for your gorilla demo service."
  default     = "timeoff"
}

variable "app_subdomain" {
  description = "The Subdomain for your application that will make CORS requests to the hasura_subdomain"
  default     = "app"
}

variable "app_image" {
  description = "Docker image that contains the application"
  type = string
}

variable "app_version" {
  description = "Application version"
  type = string
}

variable "rds_username" {
  description = "The username for RDS"
}

variable "rds_password" {
  description = "The password for RDS"
}

variable "rds_db_name" {
  description = "The DB name in the RDS instance"
}

variable "rds_port" {
  description = "The port for the DB instance"
  type = number
  default = 3306
}

variable "rds_instance_class" {
  description = "The size of RDS instance, eg db.t2.micro"
}

variable "rds_storage_encrypted" {
  description = "Whether the data on the MySQL instance should be encrpyted."
  default     = false
}

variable "az_count" {
  description = "How many AZ's to create in the VPC"
  default     = 2
}

variable "multi_az" {
  description = "Whether to deploy RDS and ECS in multi AZ mode or not"
  default     = true
}

variable "vpc_enable_dns_hostnames" {
  description = "A boolean flag to enable/disable DNS hostnames in the VPC. Defaults false."
  default     = false
}

variable "environment" {
  description = "Environment variables for ECS task: [ { name = \"foo\", value = \"bar\" }, ..]"
  default     = []
}

variable "additional_db_security_groups" {
  description = "List of Security Group IDs to have access to the RDS instance"
  default     = []
}

variable "create_iam_service_linked_role" {
  description = "Whether to create IAM service linked role for AWS ECS service. Can be only one per AWS account."
  default     = true
}

variable "ecs_cluster_name" {
  description = "The name to assign to the ECS cluster"
  default     = "gorilla-cluster"
}
