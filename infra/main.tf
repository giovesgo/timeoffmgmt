# -----------------------------------------------------------------------------
# Service role allowing AWS to manage resources required for ECS
# -----------------------------------------------------------------------------

resource "aws_iam_service_linked_role" "ecs_service" {
  aws_service_name = "ecs.amazonaws.com"
  count            = var.create_iam_service_linked_role ? 1 : 0
}

///
/// - Task execution Role
///
data "aws_iam_policy_document" "ecs_task_execution_role" {
  version = "2012-10-17"
  statement {
    sid     = ""
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name               = "gorilla-timeoff-task-execution-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_execution_role.json
}

resource "aws_iam_policy" "ecs_task_execution_role" {
  name        = "gorilla-timeoff-task-execution-policy"
  description = "Policy for trusted roles"
  policy      = file("task-execution-role-access-policy.json.tpl")
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = aws_iam_policy.ecs_task_execution_role.arn
}

///
/// - Task container Role
///
data "aws_iam_policy_document" "ecs_task_role" {
  version = "2012-10-17"
  statement {
    sid     = ""
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "ecs_task_role" {
  name               = "gorilla-timeoff-task-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_role.json
}

resource "aws_iam_policy" "ecs_task_role" {
  name        = "gorilla-timeoff-task-policy"
  description = "Policy for trusted roles"
  policy      = file("task-access-policy.json.tpl")
}

resource "aws_iam_role_policy_attachment" "ecs_task_role" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = aws_iam_policy.ecs_task_role.arn
}

# -----------------------------------------------------------------------------
# Create the certificate
# -----------------------------------------------------------------------------

resource "aws_acm_certificate" "gorilla" {
  domain_name       = "${var.gorilla_subdomain}.${var.domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

# -----------------------------------------------------------------------------
# Validate the certificate
# -----------------------------------------------------------------------------

data "aws_route53_zone" "gorilla" {
  name = "${var.domain}."
}

resource "aws_route53_record" "gorilla_validation" {
  depends_on = [aws_acm_certificate.gorilla]
  name       = element(tolist(aws_acm_certificate.gorilla.domain_validation_options), 0)["resource_record_name"]
  type       = element(tolist(aws_acm_certificate.gorilla.domain_validation_options), 0)["resource_record_type"]
  zone_id    = data.aws_route53_zone.gorilla.zone_id
  records    = [element(tolist(aws_acm_certificate.gorilla.domain_validation_options), 0)["resource_record_value"]]
  ttl        = 300
}

resource "aws_acm_certificate_validation" "gorilla" {
  certificate_arn         = aws_acm_certificate.gorilla.arn
  validation_record_fqdns = aws_route53_record.gorilla_validation.*.fqdn
}

# -----------------------------------------------------------------------------
# Create VPC
# -----------------------------------------------------------------------------

# Fetch AZs in the current region
data "aws_availability_zones" "available" {
}

resource "aws_vpc" "gorilla" {
  cidr_block           = "172.17.0.0/16"
  enable_dns_hostnames = var.vpc_enable_dns_hostnames

  tags = {
    Name = "gorilla"
  }
}

# Create var.az_count private subnets for RDS, each in a different AZ
resource "aws_subnet" "gorilla_private" {
  count             = var.az_count
  cidr_block        = cidrsubnet(aws_vpc.gorilla.cidr_block, 8, count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  vpc_id            = aws_vpc.gorilla.id

  tags = {
    Name = "gorilla #${count.index} (private)"
  }
}

# Create var.az_count public subnets for Hasura, each in a different AZ
resource "aws_subnet" "gorilla_public" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(aws_vpc.gorilla.cidr_block, 8, var.az_count + count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  vpc_id                  = aws_vpc.gorilla.id
  map_public_ip_on_launch = true

  tags = {
    Name = "gorilla #${var.az_count + count.index} (public)"
  }
}

# IGW for the public subnet
resource "aws_internet_gateway" "gorilla" {
  vpc_id = aws_vpc.gorilla.id

  tags = {
    Name = "gorilla"
  }
}

# Route the public subnet traffic through the IGW
resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.gorilla.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gorilla.id
}

# -----------------------------------------------------------------------------
# Create security groups
# -----------------------------------------------------------------------------

# Internet to ALB
resource "aws_security_group" "gorilla_alb" {
  name        = "gorilla-alb"
  description = "Allow access on port 443 only to ALB"
  vpc_id      = aws_vpc.gorilla.id

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ALB TO ECS
resource "aws_security_group" "gorilla_ecs" {
  name        = "gorilla-tasks"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.gorilla.id

  ingress {
    protocol        = "tcp"
    from_port       = "3000"
    to_port         = "3000"
    security_groups = [aws_security_group.gorilla_alb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ECS to RDS
resource "aws_security_group" "gorilla_rds" {
  name        = "gorilla-rds"
  description = "allow inbound access from the gorilla tasks only"
  vpc_id      = aws_vpc.gorilla.id

  ingress {
    protocol        = "tcp"
    from_port       = "3306"
    to_port         = "3306"
    security_groups = concat([aws_security_group.gorilla_ecs.id], var.additional_db_security_groups)
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# -----------------------------------------------------------------------------
# Create RDS
# -----------------------------------------------------------------------------

resource "aws_db_subnet_group" "gorilla" {
  name       = "gorilla"
  subnet_ids = aws_subnet.gorilla_private.*.id
}

resource "aws_db_parameter_group" "timeoff" {
  name        = "mysql-pg-gorilla"
  family      = "mysql5.7"
  description = "Parameter group for TimeOff Database"

  parameter {
    name  = "general_log"
    value = "1"
  }
  parameter {
    name  = "log_queries_not_using_indexes"
    value = "1"
  }
  parameter {
    name  = "long_query_time"
    value = "60"
  }
  parameter {
    name  = "slow_query_log"
    value = "1"
  }
  parameter {
    name         = "skip_name_resolve"
    value        = "1"
    apply_method = "pending-reboot"
  }
}

resource "aws_db_instance" "gorilla" {
  db_name                = var.rds_db_name
  identifier              = "gorilla"
  username               = var.rds_username
  password               = var.rds_password
  port                   = var.rds_port
  engine                 = "mysql"
  engine_version         = "5.7.16"
  instance_class         = var.rds_instance_class
  allocated_storage      = "10"
  storage_encrypted      = var.rds_storage_encrypted
  vpc_security_group_ids = [aws_security_group.gorilla_rds.id]
  db_subnet_group_name   = aws_db_subnet_group.gorilla.name
  parameter_group_name   = aws_db_parameter_group.timeoff.id
  multi_az               = var.multi_az
  storage_type           = "gp2"
  publicly_accessible    = false

  allow_major_version_upgrade = false
  auto_minor_version_upgrade  = false
  apply_immediately           = true
  maintenance_window          = "sun:02:00-sun:04:00"
  skip_final_snapshot         = true
  copy_tags_to_snapshot       = true
  backup_retention_period     = 7
  backup_window               = "04:00-06:00"

  lifecycle {
    #prevent_destroy = true
    prevent_destroy = false
  }
}

# -----------------------------------------------------------------------------
# Create ECS cluster
# -----------------------------------------------------------------------------

resource "aws_ecs_cluster" "gorilla" {
  name = var.ecs_cluster_name
}

# -----------------------------------------------------------------------------
# Create logging
# -----------------------------------------------------------------------------

resource "aws_cloudwatch_log_group" "gorilla" {
  name = "/ecs/gorilla"
}

# -----------------------------------------------------------------------------
# Create IAM for logging
# -----------------------------------------------------------------------------

data "aws_iam_policy_document" "gorilla_log_publishing" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:PutLogEventsBatch",
    ]

    resources = ["arn:aws:logs:${var.region}:*:log-group:/ecs/gorilla:*"]
  }
}

resource "aws_iam_policy" "gorilla_log_publishing" {
  name        = "gorilla-log-pub"
  path        = "/"
  description = "Allow publishing to cloudwach"

  policy = data.aws_iam_policy_document.gorilla_log_publishing.json
}

data "aws_iam_policy_document" "gorilla_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "gorilla_role" {
  name               = "gorilla-role"
  path               = "/system/"
  assume_role_policy = data.aws_iam_policy_document.gorilla_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "gorilla_role_log_publishing" {
  role       = aws_iam_role.gorilla_role.name
  policy_arn = aws_iam_policy.gorilla_log_publishing.arn
}

# -----------------------------------------------------------------------------
# Create a task definition
# -----------------------------------------------------------------------------

locals {
  ecs_environment = [
    {
      name  = "GORILLA_DATABASE_URI",
      value = "mysql://${var.rds_username}:${var.rds_password}@${aws_db_instance.gorilla.endpoint}/${var.rds_db_name}"
    },
    {
      name  = "GORILLA_CORS_DOMAIN",
      value = "https://${var.app_subdomain}.${var.domain}:443, https://${var.app_subdomain}.${var.domain}"
    },
    {
      name = "APP_VERSION"
      value = var.app_version
    },
    {
      name  = "NODE_ENV",
      value = "dev"
    }
  ]

  ecs_container_definitions = [
    {
      image       = var.app_image,
      name        = "timeoff",
      networkMode = "awsvpc",

      portMappings = [
        {
          containerPort = 3000,
          hostPort      = 3000
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = aws_cloudwatch_log_group.gorilla.name,
          awslogs-region        = var.region,
          awslogs-stream-prefix = "ecs"
        }
      }

      environment = flatten([local.ecs_environment, var.environment])
    }
  ]
}

resource "aws_ecs_task_definition" "gorilla" {
  family                   = "gorilla"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "256"
  memory                   = "512"
  //execution_role_arn       = aws_iam_role.gorilla_role.arn
  execution_role_arn = aws_iam_role.ecs_task_execution_role.arn

  container_definitions = jsonencode(local.ecs_container_definitions)
}

# -----------------------------------------------------------------------------
# Create the ECS service
# -----------------------------------------------------------------------------

resource "aws_ecs_service" "gorilla" {
  depends_on = [
    aws_ecs_task_definition.gorilla,
    aws_cloudwatch_log_group.gorilla,
    aws_alb_listener.gorilla,
    aws_db_instance.gorilla
  ]
  name            = "gorilla-service"
  cluster         = aws_ecs_cluster.gorilla.id
  task_definition = aws_ecs_task_definition.gorilla.arn
  desired_count   = var.multi_az == true ? "2" : "1"
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.gorilla_ecs.id]
    subnets          = aws_subnet.gorilla_public.*.id
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.gorilla.id
    container_name   = "timeoff"
    container_port   = "3000"
  }
}

# -----------------------------------------------------------------------------
# Create the ALB log bucket
# -----------------------------------------------------------------------------

resource "aws_s3_bucket" "gorilla" {
  bucket = "gorilla-${var.region}-${var.gorilla_subdomain}-${var.domain}"
  #  acl           = "private"
  force_destroy = "true"
}

# -----------------------------------------------------------------------------
# Add IAM policy to allow the ALB to log to it
# -----------------------------------------------------------------------------

data "aws_elb_service_account" "main" {
}

data "aws_iam_policy_document" "gorilla" {
  statement {
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.gorilla.arn}/alb/*"]

    principals {
      type        = "AWS"
      identifiers = [data.aws_elb_service_account.main.arn]
    }
  }
}

resource "aws_s3_bucket_policy" "gorilla" {
  bucket = aws_s3_bucket.gorilla.id
  policy = data.aws_iam_policy_document.gorilla.json
}

# -----------------------------------------------------------------------------
# Create the ALB
# -----------------------------------------------------------------------------

resource "aws_alb" "gorilla" {
  name            = "gorilla-alb"
  subnets         = aws_subnet.gorilla_public.*.id
  security_groups = [aws_security_group.gorilla_alb.id]

  access_logs {
    bucket  = aws_s3_bucket.gorilla.id
    prefix  = "alb"
    enabled = true
  }
}

# -----------------------------------------------------------------------------
# Create the ALB target group for ECS
# -----------------------------------------------------------------------------

resource "aws_alb_target_group" "gorilla" {
  name        = "gorilla-alb"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = aws_vpc.gorilla.id
  target_type = "ip"

  health_check {
    path    = "/login"
    matcher = "200"
  }
}

# -----------------------------------------------------------------------------
# Create the ALB listener
# -----------------------------------------------------------------------------

resource "aws_alb_listener" "gorilla" {
  load_balancer_arn = aws_alb.gorilla.id
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.gorilla.arn

  default_action {
    target_group_arn = aws_alb_target_group.gorilla.id
    type             = "forward"
  }
}

# -----------------------------------------------------------------------------
# Create Route 53 record to point to the ALB
# -----------------------------------------------------------------------------

resource "aws_route53_record" "gorilla" {
  zone_id = data.aws_route53_zone.gorilla.zone_id
  name    = "${var.gorilla_subdomain}.${var.domain}"
  type    = "A"

  alias {
    name                   = aws_alb.gorilla.dns_name
    zone_id                = aws_alb.gorilla.zone_id
    evaluate_target_health = true
  }
}
