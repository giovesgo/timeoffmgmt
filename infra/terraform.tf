terraform {
  backend "s3" {
    bucket         = "terraformstate-gorillademo-dev"
    dynamodb_table = "terraform-state-lock"
    encrypt        = true
    key            = "gorillademo/dev/us-east1/terraform.tfstate"
    region         = "us-east-1"
  }
  required_version = ">= 1.0.8"
}

